import ContactService from '../services/ContactService';
import ContactModel from '../models/ContactModel';

/**
 * @typedef {ContactController}
 */
export default class ContactController {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = ContactService.build();
    }

    contactModel(){
        return new ContactModel();
    }

    register( contact ) {
      let formData = new FormData();
      formData.append('name', contact.name);
      formData.append('email', contact.email);
      formData.append('phone', contact.phone);
      formData.append('message', contact.message);
      formData.append('attachment', contact.attachment);

        return this.service.create(formData).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response.rows;
            }
        );
    }

}
