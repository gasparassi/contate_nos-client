/**
 * Contact Model
 */

export default class ContactModel {

  id = null;
  name = "";
  email = "";
  phone = "";
  message = "";
  attachment = [];
  senders_ip = "";
  shipping_date = "";

  constructor(name, email, phone, message, attachment, senders_ip, shipping_date) {
    this.name = name,
    this.email = email;
    this.phone = phone;
    this.message = message;
    this.attachment = attachment;
    this.senders_ip = senders_ip;
    this.shipping_date = shipping_date;
  }

  formatDate(fullDate){
    if (!fullDate) return null;
    let date = new Date(fullDate).toISOString().substr(0, 10);
    const[year, month, day] = date.split('-');
    return `${day}.${month}.${year}`;
  }

  formatHour(fullDate){
    if (!fullDate) return null;
    let hour = new Date(fullDate).toISOString().substring(11, 19);
    return hour;
  }
  
}
