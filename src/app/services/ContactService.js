import Rest from './Rest'

/**
 * @typedef {ContactService}
 */
export default class ContactService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/contacts'

    rules = {
    required: (value) => !!value || "Este campo é obrigatório",
    emailRules: (value) => /.+@.+\..+/.test(value) || "O endereço de e-mail informado é inválido",
    minCaracterPassword: (value) => (value && value.length >= 10) || "Escreva uma mensagem com no mínime 10 caracteres",
    maxSizeFile: (value) => !value || value.size < 500000 || "O arquivo anexado precisa ser menor que 500kb",
  }

  maskTel(value){
    if (!! value) {
      return value.length == 15 ? '(##) #####-####' : '(##) ####-####';
    } else{
      return '(##) #####-####';
    }
  }

}
