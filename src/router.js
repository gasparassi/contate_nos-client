import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
	mode: 'hash',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			component: () => import('@/views/dashboard/Index'),
			children: [
				// Dashboard
				{
					name: 'dashboard',
					path: '',
					component: () => import('@/views/dashboard/Dashboard')
				},
				{
					name: 'contact-form',
					path: 'pages/contact/register',
					component: () => import('@/views/pages/contact/ContactForm'),
				},
				{
					name: 'contacts',
					path: 'pages/contact/index',
					component: () => import('@/views/pages/contact/Contacts'),
				},
			]
		}
	]
});

export default router;
